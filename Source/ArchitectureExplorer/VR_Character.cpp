// Fill out your copyright notice in the Description page of Project Settings.


#include "VR_Character.h"
#include "Camera/CameraComponent.h"

// Sets default values
AVR_Character::AVR_Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(GetRootComponent());
	UE_LOG(LogTemp, Warning, TEXT("VRChar Constructor Accessed"));
}

// Called when the game starts or when spawned
void AVR_Character::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("VR BeginPlay Accessed"));
}

// Called every frame
void AVR_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AVR_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis(TEXT("Forward"), this, &AVR_Character::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("Right"), this, &AVR_Character::MoveRight);
	UE_LOG(LogTemp, Warning, TEXT("PlayerInputSetup Accessed"));
}

void AVR_Character::MoveForward(float throttle)
{
	AddMovementInput(throttle *Camera->GetForwardVector());
	UE_LOG(LogTemp, Warning, TEXT("Forward Accessed")); 
}

void AVR_Character::MoveRight(float throttle)
{
	AddMovementInput(throttle * Camera->GetRightVector());
	UE_LOG(LogTemp, Warning, TEXT("Right Accessed"));
}